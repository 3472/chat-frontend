import React, { useState, useEffect } from 'react';
import '../css/App.css';
import "../css/Control.css";
import Chat from './Chat';
import ContactList from './ContactList';
import {
  API_CHATS, API_CONTACTS, USER, DESTINATION_PREFIX_TOPIC, DESTINATION_NEW_MESSAGE,
  DESTINATION_NEW_CONTACT, WEB_SOCKET_ADDRESS, END_POINT_CHAT, DESTINATION_USER_STATUS, ONLINE,
} from "../shared/constants";

import { Stomp } from 'stomp-websocket/lib/stomp';
import { makeGetRequest, makePostRequest, makeDeleteRequest } from '../shared/helper';
import Header from './Header';

var SockJS = require("sockjs-client");
var socket;
require("stomp-websocket");

export default function Home(props) {

  //the active chat object
  const [activeChat, setActiveChat] = useState(null);
  //the username of the logged in user
  const [loggedInUser, setLoggedInUser] = useState(null);
  //list of contacts of logged in user
  const [contacts, setContacts] = useState([]);
  //object where usernames are keys and number of unread message from each user are the values
  const [unreadMessages, setUnreadMessages] = useState({});
  //list of users that are currently logged in
  const [onlineUsers, setOnlineUsers] = useState([]);
  //the web socket subscriptions
  const [subscriptions, setSubscriptions] = useState([]);
  //used to determine if chat or contact list should be visible
  //(only on small device screens, where the "main" grid area is shared)
  const [showChat, setShowChat] = useState(false);

  //TODO make to separate useEffect for loggedInUser and activeChat?
  //run if logged in user or active chat changes
  useEffect(() => {
    //set new websocket connection
    if (loggedInUser) {
      //wasteful to set up new websocket connection each time the active chat changes,
      //but otherwise the activeChat variable inside the listeners is not up to date.

      //unsubscribe from all previous subscriptions
      subscriptions.forEach(subscription => {
        subscription.unsubscribe()
      });
      setSubscriptions([]);
      setUpWebsocket(loggedInUser);
    }
    //check if a user is already logged in
    //if yes set the logged in user and rerun this effect (but the if block instead)
    else {
      loadLoggedInUser();
    }
  }, [loggedInUser, activeChat])

  return (
    <div className="grid-container">
      <div className="contact-view">
        <ContactList
          contacts={contacts}
          unreadMessages={unreadMessages}
          onlineUsers={onlineUsers}
          handleContactClick={displayContact}
          handleAddContact={handleAddContact}
          handleDeleteContact={handleDeleteContact}
        />
      </div>

      <div className={showChat ? "chat-view chat-visible" : "chat-view"}>
        <Chat
          loggedInUser={loggedInUser} 
          activeChat={activeChat}
          triggerChatReload={reloadChat}
          handleBackClick={() => setShowChat(false)}
        />
      </div>

      <div className="control-view">
        <div className="control">
          <Header
            loggedInUser={loggedInUser}
            handleLogin={handleLogin}
            handleLogout={handleLogout}
          />
        </div>
      </div>
    </div>
  );

  //update the active chat
  function reloadChat() {
    makeGetRequest(`${API_CHATS}/${activeChat.id}`, setActiveChat);
  }

  //update state and load further data
  function handleLogin(username) {
    setLoggedInUser(username);
    loadContacts();
    loadOnlineUsers();
  }

  //update state
  function handleLogout() {
    setLoggedInUser(null);
    setActiveChat(null);
    setContacts([]);
  }

  //chat with selected contact becomes active chat
  function displayContact(username) {
    setShowChat(true);
    loadChatWithUser(username);
    resetUnreadMessages(username);

    //load chat with the selected user and update state
    function loadChatWithUser(username) {
      makeGetRequest(`${API_CHATS}?username=${username}`, setActiveChat);
    }

    function resetUnreadMessages(username) {
      setUnreadMessages({ ...unreadMessages, [username]: 0 });
    }
  }

  //send a post request with the username (contactIdentifier) to be added to the server
  //and afterwards reload the contact list
  function handleAddContact(contactIdentifier) {
    makePostRequest(`${API_CONTACTS}/?username=${contactIdentifier}`, loadContacts);
  }

  //send a request to delete the contact specified by username from the contact list of the logged in user 
  function handleDeleteContact(username) {
    console.log("delete contact");
    makeDeleteRequest(`${API_CONTACTS}/?username=${username}`, loadContacts);
  }

  //send request to the server to return the currently logged in user for this client (by checking the cookies).
  //the server will answer with {user: username} if username is logged in, or {} if no user is logged in
  //if a user is logged in update the state and load further data
  function loadLoggedInUser() {
    makeGetRequest(USER,
      json => {
        if (json && json.user) {
          setLoggedInUser(json.user);
          loadContacts();
          loadOnlineUsers();
        }
      })
  }

  //request the contact list for the currently logged in user from the server and update the state
  function loadContacts() {
    return makeGetRequest(API_CONTACTS, (json) => setContacts(Array.isArray(json) ? json : []));
  }

  //load all users that are currently online and update the state
  function loadOnlineUsers() {
    makeGetRequest(ONLINE, setOnlineUsers);
  }

  //create a stomp connection via websocket and set up listeners for this client (logged in user).
  function setUpWebsocket(username) {
    console.log("setup websocket");
    //close existing connection;
    if (socket) {
      socket.close();
    }
    let port = "";
    if(window.location.hostname == "localhost") {
      port = ":8080";
    }
    socket = SockJS(`${window.location.protocol}//${window.location.hostname}${port}/${END_POINT_CHAT}`);
    let stompClient = Stomp.over(socket);
    stompClient.connect({}, (frame) => {
      console.log("Stomp client connected: ", frame);
      //Listen for when the logged in user is added as a contact and reload the contact list
      registerSubscription(stompClient, `/${DESTINATION_PREFIX_TOPIC}/${DESTINATION_NEW_CONTACT}/${username}`,
        (message) => loadContacts());
      //Listen for when a new message is sent to the logged in user 
      //and reload the chat (chatid is sent back in the message body)
      registerSubscription(stompClient, `/${DESTINATION_PREFIX_TOPIC}/${DESTINATION_NEW_MESSAGE}/${username}`,
        (message) => {
          const { chatId, username } = JSON.parse(message.body);
          if (activeChat && activeChat.id === chatId) {
            //update the active chat to display the new messages
            reloadChat();
          }
          else {
            //increase the unread messages for the contact that sent the message
            console.log("new unread message");
            let newCount = unreadMessages[username] ? unreadMessages[username] + 1 : 1;
            setUnreadMessages({ ...unreadMessages, [username]: newCount });
          }
        });

      registerSubscription(stompClient, `/${DESTINATION_PREFIX_TOPIC}/${DESTINATION_USER_STATUS}`,
        (message) => {
          const { username, loggedIn } = JSON.parse(message.body);
          if (loggedIn) {
            setOnlineUsers([...onlineUsers, username]);
          }
          else {
            setOnlineUsers(onlineUsers.filter(user => user != username));
          }
        });
    });

    //register a subscription and add it to the state to unsubscribe later
    function registerSubscription(stompClient, route, callback) {
      const subscription = stompClient.subscribe(route, callback);
      setSubscriptions([...subscriptions, subscription]);
    }
  }
}
