import React, { useState, useRef } from "react";
import ImageUpload from "./ImageUpload";

export default function Input(props) {
    const [message, setMessage] = useState("");
    const [image, setImage] = useState(null);
    const formRef = useRef(null);
    return (
        <div>
            <form ref={formRef}>
                <textarea
                    name="input"
                    value={message}
                    onChange={(event) => setMessage(event.target.value)}
                />
                <button onClick={onSend}>
                    Send
                        </button>
                <ImageUpload handleImageSelected={(image) => setImage(image)} />
            </form>
        </div>
    );

    function onSend(event) {
        event.preventDefault();
        props.handleMessageSend(message, image);

        //clear the fields
        setMessage("");
        setImage(null);
        formRef.current.reset();
    }
}

