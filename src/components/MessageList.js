import React, {useRef, useEffect} from "react";
import Message from "./Message";

//render the messages in ascending date / time order (oldest at the top, newest at the bottom)
//the ul element is styled with a fixed height and overflow auto in order to make it scrollable

export default function MessageList(props) {
    //reference to the list element
    const listRef = useRef(null);

    //scroll down to the bottom inside the list element
    useEffect( () => {
        if(listRef) {
            listRef.current.scrollTop = listRef.current.scrollHeight;
        }
    });

    const messages = props.messages.sort((a, b) => {
        return compareMessageDateTime(a, b);
    }).map(m => {
        return (
            <li
                key={m.id}>
                <Message
                    loggedInUser={props.loggedInUser}
                    username={m.user.username}
                    text={m.text}
                    date={formatDate(m.date)}
                    time={formatTime(m.time)}
                    imageLocation={m.imageLocation}
                />
            </li>
        )
    })
    return (
        <ul ref={listRef}>
            {messages}
        </ul>
    )
}

//compare two messages by their date and time values
function compareMessageDateTime(a, b) {
    const date1 = parseDateTime(a.date, a.time);
    const date2 = parseDateTime(b.date, b.time);
    return date1 - date2;
}

//converts date and time to a javascript date object
function parseDateTime(date, time) {
    const [year, month, day] = date.split("-");
    const [hour, minute, second] = time.split(":");
    const d = new Date(year, month - 1, day, hour, minute, second);
    return d;
}

//returns a formated date string
function formatDate(date) {
    const [year, month, day] = date.split("-");
    const now = new Date();
    const thisYear = now.getFullYear();
    const thisMonth = now.getMonth()+1;
    const thisDay = now.getUTCDate();
    if(year == thisYear && month == thisMonth) {
        if(day == thisDay) {
            return "Today";
        }
        else if(thisDay == (Number(day) + 1)) {
            return "Yesterday";
        }
        return `${day}.${month-1}`;
    }
    return `${day}.${month-1}.${year}`;
}

//returns a formated time string
function formatTime(time) {
    const [hour, minute, second] = time.split(":");
    return `${hour}:${minute}`;
}
