import React, { useState } from "react"
import { PROFILE_PICTURE_HEIGHT, PROFILE_PICTURE_WIDTH } from "../shared/constants";
import "../css/Contact-list.css";
import Modal from "./Modal";

//display the contact username, profile picture, his status (online or not) and the number of unread messages (if > 0)
//a click to the delete button will display a modal dialog, which is otherwise hidden
export default function Contact(props) {
    const { unreadMessages, username, userStatus, profilePictureLocation, handleContactClick,
        handleDeleteContact } = props;

    //track if the delete modal dialog should be displayed
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    return (
        <div className="contact"
            onClick={() => handleContactClick(username)}
        >
            <img
                src={profilePictureLocation}
                alt="profile"
            />
            <span className="name">
                {username}
            </span>

            <span className="unread-messages">
                {unreadMessages > 0 &&
                    <b>
                        {unreadMessages}
                    </b>
                }
            </span>

            <span className={"status" + (userStatus ? " online" : "")}>
            </span>
            <span>
                <button
                    className="delete-button"
                    onClick={() => setShowDeleteModal(true)}
                >Delete</button>
            </span>
            {createDeleteModal()}
        </div>
    );

    //creates a delete dialog and sets its css class depending on the showDeleteModal state
    //if visible (showDeleteModal = true) it is styled with the modal class.
    //if not visible, it will be hidden with the hide class
    function createDeleteModal() {
        const MESSAGE = "Do you really want to delete this contact?";
        const YES = "Yes";
        const NO = "No";
        return (
            <span className={showDeleteModal ? "modal" : "hide"}>
                <Modal>
                    <p>
                        {MESSAGE}
                    </p>
                    <button onClick={() => {
                        handleDeleteContact(username)
                        setShowDeleteModal(false);
                    }}>
                        {YES}
                    </button>
                    <button onClick={() => setShowDeleteModal(false)}>
                        {NO}
                    </button>
                </Modal>
            </span>
        );
    }
}
