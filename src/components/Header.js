import React from "react";
import UserControl from "./UserControl";
import "../css/Control.css";

export default function Header(props) {

    const { loggedInUser, handleLogin, handleLogout } = props;

    return (
        <UserControl
            loggedInUser={loggedInUser}
            handleLogin={handleLogin}
            handleLogout={handleLogout}
        />

    )
}