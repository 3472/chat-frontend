import React, { useState } from "react";
import ImageUpload from "./ImageUpload";
import { makePostRequestWithData } from "../shared/helper";
import "../css/Control.css";

export default function Settings(props) {
    const [profilePicture, setProfilePicture] = useState(null);
    return (
        <div className="settings">
            <h3>Settings</h3>
            <form>
                <ImageUpload handleImageSelected={(image) => setProfilePicture(image)} />
                <br />
                <button onClick={sendSettings}>
                    Upload
                </button>
            </form>
        </div>
    );

    //TODO resize image before upload to save space and bandwidth
    //TODO use username or similar as part of key
    function sendSettings(event) {
        event.preventDefault();
        let formData = new FormData();
        formData.append("picture", profilePicture);
        formData.append("key", "settings");
        makePostRequestWithData("/upload", null, formData);
    }
}



