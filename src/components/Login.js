import React, { useState, useEffect } from "react";
import { API_LOGIN } from "../shared/constants";
import { makePostRequestWithData } from "../shared/helper";
import { Link } from "react-router-dom";

export default function Login(props) {
    //TODO render login error
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(null);

    console.log("error: ", error);

    let errorMsg;
    if (error) {
        errorMsg = <div style={{ color: "red" }}>{error.message}</div>
    }
    return (
        <span>
            <div className="login">
                {errorMsg}
                <form>
                    <label>
                        Username:
                        <input type="text" name="username" value={username}
                            onChange={(event) => setUsername(event.target.value)}>
                        </input>
                    </label>
                    <label>
                        Password:
                        <input type="password" name="password" value={password}
                            onChange={(event) => setPassword(event.target.value)}>
                        </input>
                    </label>
                    <button onClick={onLoginClick}>Login</button>
                </form>
                <div className="link">
                    <Link to="/register">Register</Link>
                </div>
            </div>
        </span>
    )

    //send login request with username and password in the request body
    function onLoginClick(event) {
        event.preventDefault();
        let formData = new FormData();
        formData.append("username", username);
        formData.append("password", password);

        makePostRequestWithData(API_LOGIN,
            (res) => {
                setError(null);
                props.handleLogin(username)
            },
            new URLSearchParams(formData),
            (err) => {
                console.log("setError");
                setError(err);
            });
    }
}

