import React, { useState } from "react";
import { API_REGISTER, USERS, API_USERS, VALID_EMAIL_REGEX } from "../shared/constants";
import { makePostRequestWithData, makeGetRequest } from "../shared/helper";
import "../css/App.css";

export default function Register(props) {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [passwordConfirmation, setPasswordConfirmation] = useState("");
    const [errors, setErrors] = useState({ username, email, password });

    return (
        <div className="register">
            <form>
                <label>
                    <span>
                        {errors.username.length > 0 &&
                            <ul className="error">{errors.username}</ul>}
                    </span>
                    Username:
                <input type="text" name="username" value={username}
                        onChange={handleChange}>
                    </input>

                </label>


                <label>
                    <span>
                        {errors.email.length > 0 &&
                            <ul className="error">{errors.email}</ul>}
                    </span>
                    Email:
                <input type="email" name="email" value={email}
                        onChange={handleChange}>
                    </input>

                </label>

                <label>
                    <span>
                        {errors.password.length > 0 &&
                            <ul className="error">{errors.password}</ul>}
                    </span>
                    Password:
                <input type="password" name="password" value={password}
                        onChange={handleChange}>
                    </input>
                </label>

                <label>
                    <span>
                        {errors.password.length > 0 &&
                            <ul className="error">{errors.password}</ul>}
                    </span>
                    Confirm Password:
                <input type="password" name="passwordConfirmation"
                        value={passwordConfirmation}
                        onChange={handleChange}>
                    </input>

                </label>

                <button onClick={onRegisterClick}>
                    Register
                </button>
            </form>
        </div>
    );

    function handleChange(event) {
        const { name, value } = event.target;

        switch (name) {
            case "passwordConfirmation":
                validatePassword(value);
                setPasswordConfirmation(value);
                break;
            case "password":
                validatePassword(value);
                setPassword(value);
                break;
            case "username":
                validateUsername(value);
                setUsername(value);
                break;
            case "email":
                validateEmail(value);
                setEmail(value);
                break;
            default:
        }
    }

    function validatePassword(pwd) {
        const MIN_LENGTH = 1
        const tooShortError = pwd.length >= MIN_LENGTH ? "" : "Password needs to be at least " + MIN_LENGTH +
            " Characters long";
        const notIdenticalError =
            (pwd === passwordConfirmation || pwd === password) ? "" : "Passwords do not match";
        setErrors({
            ...errors,
            "password": [tooShortError, notIdenticalError].filter(e => e != "")
                .map(e => <li key={e}>{e}</li>)
        });
    }

    function validateUsername(username) {
        const MIN_LENGTH = 1;
        const tooShortError = username.length >= MIN_LENGTH ? "" : "username needs to be at least" + MIN_LENGTH +
            " Characters long";
        setErrors({
            ...errors,
            "username": tooShortError
        })
    }

    //check sever, if username already registered for another user
    async function checkUsernameAvailable(username) {
        makeGetRequest(`${API_USERS}/${username}`,
            json => json.status || json.status === 404 ||
                setErrors({
                    ...errors, "username": "username not available"
                })
        );
    }

    function validateEmail(email) {
        setErrors({
            ...errors,
            "email": VALID_EMAIL_REGEX.test(email) ? "" : "Not a valid email address"
        });
    }

    //check if username is available and no errors occurred before sending the registration
    async function onRegisterClick(event) {
        event.preventDefault();
        await checkUsernameAvailable(username);
        //if any form field has errors, cancel the submit process
        if (errors.username.length > 0
            || errors.password.length > 0
            || errors.email.length > 0) {
            return;
        }
        sendRegistration();
    }

    //send a registration request with the username, email and password in the request body to the server
    //if response comes back successful redirect the user to the home page
    async function sendRegistration() {
        let formData = new FormData();
        formData.append("username", username);
        formData.append("email", email);
        formData.append("password", password);

        makePostRequestWithData(API_REGISTER,
            res => {
                console.log("Successfully registered");
                props.history.push("/");
            },
            new URLSearchParams(formData)
        );
    }

}

