import React from "react"
import MessageList from "./MessageList";
import Input from './Input';
import "../css/Chat.css";
import { API_MESSAGES } from "../shared/constants";
import { makePostRequestWithData } from "../shared/helper";

export default function Chat(props) {

    const { activeChat, loggedInUser, handleBackClick } = props;

    if (!activeChat) {
        return (
            <div>
            </div>
        );
    }
    const otherUser = activeChat.users.filter(user => user.username != loggedInUser)[0];
    return (
        <div className="chat">
            <button className="back-button" onClick={handleBackClick}>Back</button>
            <div className="header">
                <img
                    src={otherUser.profilePictureLocation}
                    alt="profile"
                />
                <div className="title">
                    Chat with {otherUser.username}
                </div>
            </div>
            <br />
            <div className="message-list">
                <MessageList
                    loggedInUser={loggedInUser}
                    messages={activeChat.messages || []}
                />
            </div>
            <div className="message-input">
                <Input
                    handleMessageSend={handleMessageSend} />
            </div>
        </div >
    );

    function handleMessageSend(message, image) {
        sendMessage(message, props.activeChat.name, image);
    }


    //send a message with text, image and chatname in the request body to the server
    //and reload the chat when the response comes back
    function sendMessage(message, chatName, image) {
        let formData = new FormData();
        formData.append("text", message);
        formData.append("chatName", chatName);
        formData.append("image", image);
        makePostRequestWithData(API_MESSAGES,
            () => {
                props.triggerChatReload();
            },
            formData);
    }

}