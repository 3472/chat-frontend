import React from "react"
import { MESSAGE_PICTURE_HEIGHT, MESSAGE_PICTURE_WIDTH } from "../shared/constants";

//display the username, the text, the date, the time and if present the image of a message
//sets the css classes according to the user who posted it (loggedInUser or the other)
export default function Message(props) {
    const { text, imageLocation, username, loggedInUser, date, time } = props;
    const isFromLoggedInUser = loggedInUser === username;
    return (
        <div>
            <div className={"message " + (isFromLoggedInUser ? "" : "other-message")}>
                {text}

                {imageLocation &&
                    <a
                        href={imageLocation}>
                        <img
                            src={imageLocation} alt="sent with message"
                        />
                    </a>
                }

            </div>
            <div className={isFromLoggedInUser ? "author" : "other-author"}>
                {username}, {time}, {date}
            </div>
        </div>
    );
}
