import React from "react";
import { API_LOGOUT } from "../shared/constants";
import { makePostRequest } from "../shared/helper";
import "../css/Control.css";

export default function Logout(props) {
    return (
        <div className="logout">
            {props.loggedInUser}
            <button onClick={onLogoutClick}>Logout</button>
        </div>
    );


    //send request to logout
    function onLogoutClick() {
        return makePostRequest(API_LOGOUT, res => {
            console.log("Successfully logged out");
            props.handleLogout();
        });
    }
}


