import React from 'react';
import { Switch, Route } from "react-router-dom";
import '../css/App.css';
import Register from './Register';
import Home from './Home';
import Settings from './Settings';

export default function App() {
  return (
    <div >
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/register" component={Register} />
        <Route path="/settings" component={Settings} />
      </Switch>
    </div>
  );
}