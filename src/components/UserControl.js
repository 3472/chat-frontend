import React from "react";
import Login from "./Login";
import Logout from "./Logout";
import { Link } from "react-router-dom";

//if a user is logged in (props.loggedInUser != "") then display the logout component
//otherwise display the login and register components
export default function UserControl(props) {
    const { loggedInUser, handleLogin, handleLogout } = props;

    return loggedInUser
        ? (
            <div className="user-control">
                <Logout
                    loggedInUser={loggedInUser}
                    handleLogout={handleLogout} />
            </div>

        )
        : (
            <div className="user-control">
                <Login handleLogin={handleLogin} />
            </div>
        )
}