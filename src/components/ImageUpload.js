import React from "react";

//TODO style button (select file)
//TODO Allow more than one image
export default function ImageUpload(props) {
    return (
        <div>
            <label>
                Upload Picture
                    </label>
            <input type="file" name="picture" onChange={onFileChange}>
            </input>
        </div>
    );

    //update the selected image in the parent component
    //TODO make filefilter (only images)
    function onFileChange(event) {
        props.handleImageSelected(event.target.files[0]);
    }
}

