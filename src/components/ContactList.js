import React, { useState } from "react"
import Contact from "./Contact";
import "../css/Contact-list.css";

//displays a list of contacts
//the currently selected contact is styled differently (with the selected class)
export default function ContactList(props) {
    const [contactIdentifier, setContactIdentifier] = useState("");
    const [selectedContact, setSelectedContact] = useState("");
    const { unreadMessages, onlineUsers } = props;
    const contacts = props.contacts.map(contact => {
        const { username, id, profilePictureLocation } = contact;
        const userStatus = onlineUsers.filter(user => username == user)[0];
        return (<li
            className={(selectedContact === username ? "selected" : "")}
            key={id}
        >
            <Contact
                username={username}
                profilePictureLocation={profilePictureLocation}
                unreadMessages={unreadMessages[username]}
                userStatus={userStatus}
                handleContactClick={(username) => {
                    props.handleContactClick(username);
                    setSelectedContact(username);
                }}
                handleDeleteContact={(username) => {
                    props.handleDeleteContact(username);
                }}
            />
        </li>)
    });

    return (
        <div>
            {createSearch()}
            <ul className="contact-list">
                {contacts}
            </ul>
        </div>
    );

    function createSearch(text, buttonLabel, clickHandler, changeHandler) {
        const SEARCH_TEXT = "Find friends by username or email";
        return (
            <div className="search">
                <form>
                    <label>
                        {SEARCH_TEXT}
                    </label>
                    <br />
                    <input
                        type="text"
                        name="contactIdentifier"
                        value={contactIdentifier}
                        onChange={(event) => setContactIdentifier(event.target.value)}
                    />
                    <button onClick={onAddClick}>
                        Add Contact
                    </button>
                </form>
            </div>
        );
    }
    
    function onAddClick(event) {
        event.preventDefault();
        props.handleAddContact(contactIdentifier);
        setContactIdentifier("");
    }
}