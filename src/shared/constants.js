
export const API = "api";

export const CONTACTS = "contacts";
export const API_CONTACTS = `/${API}/${CONTACTS}`;

export const USERS = "users";
export const API_USERS = `/${API}/${USERS}`;

export const CHATS = "chats";
export const API_CHATS = `/${API}/${CHATS}`;

export const MESSAGES = "messages";
export const API_MESSAGES = `/${API}/${MESSAGES}`;

export const ONLINE = "online";
export const API_ONLINE = `${ONLINE}`;

export const API_LOGOUT = `/logout`;
export const API_LOGIN = `/login`;
export const API_REGISTER = `/register`;


export const USER = "/user";



//websocket constants
export const WEB_SOCKET_ADDRESS = "http://localhost:8080";
export const END_POINT_CHAT = "chat";
export const DESTINATION_PREFIX_TOPIC = "topic";
export const DESTINATION_PREFIX_APP = "app";
export const DESTINATION_NEW_MESSAGE = "newMessage";
export const DESTINATION_NEW_CONTACT = "newContact";
export const DESTINATION_USER_STATUS = "userStatus";





//image constants
export const PROFILE_PICTURE_WIDTH = 50;
export const PROFILE_PICTURE_HEIGHT = 50;

export const MESSAGE_PICTURE_WIDTH = 100;
export const MESSAGE_PICTURE_HEIGHT = 100;


export const VALID_EMAIL_REGEX = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
