//make a get request to the supplied url and execute the supplied callback
//function on the json response body
export function makeGetRequest(url, callback) {
    fetch(url)
        .then(res => {
            return res.json();
        })
        .then(json => {
            if (json) {
                callback(json);
            }
        }).catch(err => console.error("Get request returned error: ", err));
}


//make a post request to the supplied url and execute the supplied callback function on the response
export function makePostRequest(url, callback, errorCallback) {
    return makePostRequestWithData(url, callback, null, errorCallback);
}


//make a post request to the supplied url with the supplied data in the body.
//Execute the the supplied callback function on the response
export function makePostRequestWithData(url, callback, data, errorCallback) {
    fetch(url, {
        method: "POST",
        body: data,
    })
        .then(res => callback(res))
        .catch(err => {
            console.error("Post request returned error: ", err)
            errorCallback(err);
        });
}

export function makeDeleteRequest(url, callback, errorCallback) {
    fetch(url, {
        method: "DELETE"
    })
        .then(res => callback(res))
        .catch(err => {
            console.error("Delete request returned error: ", err)
            errorCallback(err);
        });
}
